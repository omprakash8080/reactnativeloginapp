import React, { Component } from 'react';
import { Container, Footer, FooterTab, Text,
Content, Header, Left, Icon,
Body, Title, Right,
Card, CardItem,
Form, Item, Input, Label, Button,
InputGroup, 
} from 'native-base';


import {
  AppRegistry,
  Alert

} from 'react-native';


AppRegistry.registerComponent('ReactHelloWorld', () => ReactHelloWorld);

export default class ReactHelloWorld extends Component {
  
  constructor(){
    super();

    this.state = {
      email: "",
      password: "",
      error: "",
      

    }
  }

  
  repoList(){
  

  if(this.state.email == this.state.password){

    Alert.alert(
      'Login',
      'Login Successfull !!',
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ]
    );
  }
      
  }

  render() {
    return (
      <Container>

                <Header>
                  <Left><Button transparent>
                        <Icon name='train' />
                        </Button>
                  </Left>

                  <Body><Title>ReactHelloWorld</Title></Body>

                  <Right><Button transparent>
                         <Icon name='menu' />
                        </Button>
                  </Right>
                </Header>

        <Content padder>

          <Card><CardItem><Body >
                
              <Text style= {{textAlignVertical: "center", 
              textAlign: "center", width: '100%'
          }}>

              
                   React Native + Native Base 
                </Text>

                

              </Body></CardItem></Card>

            <Form style= {{marginTop: 40}}>


                <Item >
                  <Input placeholder="Username"
                  placeholderTextColor="#00897b"
                  onChangeText={ (text)=> this.setState({email: text}) }/>
                  
                </Item>


                <Item last>
                  <Input placeholder="Password"
                  placeholderTextColor="#00897b"
                  onChangeText={ (text)=> this.setState({password: text}) }/>
                </Item>


            <Button full onPress= {() =>{ this.repoList();}} 
             style= {{marginTop: 40}}>
            <Text>Login</Text>
            </Button>

        </Form> 
      

        </Content>


        <Footer>
          <FooterTab>
            <Button full>
              <Text>Footer</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
