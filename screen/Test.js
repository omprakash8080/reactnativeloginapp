import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Button,
  Alert,
  Text
} from 'react-native';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  _textCss: {
    color: '#ffffff',
    backgroundColor: '#00897b',
    margin: 10,
    padding: 10
    
  },

  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default class Test extends Component {

  render() {
    return (
      <View style={styles.container}>

        <Text style={styles._textCss}>Test Page</Text>

    

      </View>
    );
  }


}